package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class SptingwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SptingwebApplication.class, args);
	}

	@RequestMapping("/data")
	public String home() {
	return "Spring boot is working!";
	}
}
