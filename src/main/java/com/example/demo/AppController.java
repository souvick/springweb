package com.example.demo;

import java.util.ArrayList;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class AppController {
	static ArrayList<Employee> empList = new ArrayList<>();

	
	@RequestMapping("/employeelist")
	public String employeelist(Model model) {
		
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		
		e1.setId("00001");
		e1.setName("Employee1");
		
		e2.setId("00002");
		e2.setName("Employee2");
		
		
		empList.add(e1);
		empList.add(e2);

		
		
		model.addAttribute("time", System.currentTimeMillis());
		model.addAttribute("empList", empList);

		return "employeelist";
	}
	
	@RequestMapping("/addEmployee")
	public String addEmployee(Model model) {
		
		model.addAttribute("time", System.currentTimeMillis());
		return "employee";
	}
	
	@RequestMapping(value = "/submitemployee", method = RequestMethod.POST,
	        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String submitemployee(Employee emp) {
		
		empList.add(emp);
		
		return "redirect:/employeelist";
	}
	
	
}